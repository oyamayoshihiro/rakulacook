class ManuPagesController < ApplicationController


  def init
    #  ログイン済かそうでないかチェック
    #  ログイン済：ユーザDBからアレルギー値を取得し、def questionへ
    #  非ログインの場合はアレルギーをDBから取得し、チェックボックスへ格納
    if logged_in?
      current_user.alvalue
      redirect_to :action => 'question'
    else
      @allergie = Allergy.all
  end

    if params[:back]
      render :text => 'テキスト'
    end
  end


  def question

    # セッション変数の初期化
    session[:maindish] = ""
    session[:subdish] = ""
    session[:people] = ""
    #  情報をfindに渡す
    # helper_method :current_user, :logged_in?
    # if logged_in?
    #   current_user.alvalue
    #   redirect_to :action => 'question'
    # else
       @all = Allergy.all
    #session[:allergy] = allergy[]
    #session[:alle] = params[:allergy]
    # end

     if params[:back]
       render :text => 'テキスト'
     end

    #  ジャンル選択用

    @s_genre = Genre.select("id,jname")

    # ユーザが選んだ情報を取得する
    @allergy = params[:allergy]
    if logged_in?
       if not current_user.alvalue.nil?
         @allergy = current_user.alvalue.split(",")
       end
      # @allergy = [2,3,4]
    else
      # @allergy = [2,3,4]
      # @allergy = params[:allergy]
    end
    #@all = Allergy.all
    #  情報をfindに渡す
    @i = 1;
    if @allergy != nil
      array = Array.new
      @allergy.each.with_index(1) do |u|
        array.push("alvalue NOT like '%,#{u},%'")		end
          sql = "select * from dishes where "
          array.each do |a|
            if @i == 1
            sql = sql + a
            @i = 2
          else
            sql = sql +" or "+ a
         end
       end
       else
         sql = "select * from dishes where id IN"

       end
    @alsql = sql

end


   def find
     # 入力された値から献立番号(料理番号)を検索し、レシピ表示コントローラに渡す

   end


  def question_result
    # コネクションを貼る
    con = ActiveRecord::Base.connection
    base_sql = "select id from dishes where "

    # アレルギーを除くためのLike演算子の生成
    like_text = ""
    if not params[:allergy].nil?
     like_text = ""
     params[:allergy].each do |allergy_id|
       like_text += "alvalue not like '%,#{allergy_id},%' and "
     end
    end

    # 4:時間の質問
    # 時間に余裕があるか否かを尋ねる
    if params[:time][:time] == 'yes'
     # すべての料理表示
     genre_sql = "genre_id = #{params[:genre]}"
     time_sql = ""
    else
     # 時間余裕なし、step数少なめのみ表示
     genre_sql = "genre_id = #{params[:genre]} and"
     time_sql = " recipe NOT like '%step5%'"
    end

    # ジャンルと時間の設定のみ
    # 主菜候補の抽出
    sql = base_sql + like_text + " flag = 0 and " + genre_sql + time_sql + ";"
    @main_candidate = con.select_values(sql).sample

    if @main_candidate.nil?
      session[:maindish] = 0
    else
      session[:maindish] = @main_candidate
    end

    # 副菜候補の抽出
    sql = base_sql + like_text + " flag = 1 and " + genre_sql + time_sql + ";"
    @sub_candidate = con.select_values(sql)

    if @sub_candidate.nil?
      session[:subdish] = 0
    else
      session[:subdish] = @sub_candidate.sample(params[:side][:number].to_i)
    end
    session[:people] = params[:person][:number]
    redirect_to dish_path
    #データの格納ここまで
  end
end
